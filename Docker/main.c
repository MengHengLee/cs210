
#include <stdio.h>
#include "math.h"
int main() {
    float num1 = 0;
    float num2 = 0;
    char op = ' ';
    int foo = 999;
    float result = 0;
    do {
        printf("Please enter your operation:\t");
        scanf("%f%c%f", &num1, &op, &num2);
        switch(op) {
            case '+':
                result = add(num1,num2);
                break;
            case '-':
                result = subtract(num1,num2);
                break;
            case '*':
                result = multiply(num1,num2);
                break;
            case '/':
                result = divide(num1,num2);
                break;
            case '^':
                result = power(num1,num2);
                break;
            case '$':
                result = recursivePower(num1,num2);
                break;
            default:
                printf("the operation you entered does not work");
                break;
        }
        printf("<%.2f>\n", result);
    } while (foo == 999);

}
