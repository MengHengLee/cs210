//
// Created by Donny Lee on 2021/2/15.
//

float add(float num1, float num2){
    float result = num1 + num2;
    return result;
}

float subtract(float num1, float num2){
    float result = num1 - num2;
    return result;
}

float multiply(float num1, float num2){
    float result = num1 * num2;
    return result;
}

float divide(float num1, float num2){
    float result = num1 / num2;
    return result;
}

float power(float num1, float num2){
    float result = num1;
    for(int foo = 0; foo < num2-1; foo++){
        result = result*num1;
    }
    return result;
}

float recursivePower(float num1, float num2){
    if (num2 == 1){
        return num1;
    }
    return (recursivePower(num1, num2-1)*num1);
}